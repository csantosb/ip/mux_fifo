#+TITLE: Implement with vivado

* Implement

** Vivado (manually)

Create a project. Include these filesets

#+begin_src sh
  []
#+end_src

as defined in =mux_fifo.core= file. Then, proceed as usual.

** Vivado (provided files)

Just run

#+begin_src sh
  cd prj_vivado19.2
  ./implement.sh
#+end_src

** Vivado (fusesoc)

Use provided =implement.fusesoc.sh= scrip file

#+begin_src sh
  cd prj_vivado19.2
  ./implement.fusesoc.sh
#+end_src
