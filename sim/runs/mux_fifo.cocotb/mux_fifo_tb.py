#!/usr/bin/python

'''
A driver injects data randomly in a set of NB_CHANNELS parallel fifos.
DUT is responsible for transferring this data to a common out fifo.
Then, a monitor reads this data from the dut. As data increases by +1 by fifo,
when reading data a test is performed to check its validity.

NB_EVENTS are sent, in bursts with MULTIPLICITY number of events in a burst.
'''

# * Imports

import queue
import os
import cocotb
from cocotb.triggers import Timer, FallingEdge, RisingEdge, with_timeout, First, Join
from cocotb.clock import Clock
from cocotb.utils import get_sim_time
from cocotb import fork
from cocotb.result import SimTimeoutError, TestSuccess, SimFailure, TestFailure
from random import uniform
from edacommonlib.testbench.testbench import tb_base


class SimNoDataException(Exception):
    """
    Custom exception signaling missing incoming data.
    """

    def __init__(self, message):
        self.message = message

# * Testbench class


class mux_fifo_tb(tb_base):
    """
    Implements infrastructure for testing mux_fifo dut.
    """

    def __init__(self, dut):
        """
        Initialize things.
        """

        tb_base.__init__(self, dut)

        # access and store dut generics

        # i. get generics from dut
        self.NB_CHANNELS = dut.g_nb_channels.value.integer
        self.DATA_WIDTH = dut.g_data_width.value.integer
        self.MAX_INTERVAL = dut.g_max_interval.value.integer
        self.MIN_INTERVAL = dut.g_min_interval.value.integer

        # ii. get generics as env variables
        # self.NB_CHANNELS = int(os.environ['NB_CHANNELS'])
        # self.DATA_WIDTH = int(os.environ['DATA_WIDTH'])
        self.COVERAGE_SAMPLES_OSVVM = os.environ['COVERAGE_SAMPLES_OSVVM']
        self.TIMEOUT = int(os.environ['TIMEOUT'])

        # TODO: Fix that
        # multiplicity of one burst
        self.MULTIPLICITY = int(os.environ['MULTIPLICITY'])

        self.NbEventsSent = 0
        self.NbEventsCaptured = 0

        # init inputs
        dut.fifo_out_rd_en <= 0
        dut.osvvm_do_report <= 0
        dut.fifo_in_wr_ext <= 0
        dut.fifo_in_data_ext <= 0
        dut.osvvm_do_stimulus <= 0

        # initial reset
        # self.reset_task = fork(self.tb_base_reset())

        # TODO: replace by socreboard
        # create an array of NB_CHANNELS fifos for storing injected data
        self.fifo_data = [queue.Queue() for i in range(self.NB_CHANNELS)]

    # def setup(self):
    #     """
    #     Setup Simulation.
    #     """

    #     import os
    #     self.current_folder = os.path.dirname(os.path.abspath(__file__))
    #     import subprocess
    #     # sets constant in tb package as a workaround to using a generic (ghdl
    #     # crashes)
    #     subprocess.call(["sed", "-i",
    #                      's/constant nb_fifoself.*;/constant nb_fifos : natural := {};/'.
    #                      format(self.NB_CHANNELS),
    #                      self.current_folder + '/hdl/mux_fifo_tb_pkg.vhd'])

    async def driver(self):
        """
        Produce data in bursts. One burst may contain several events, given by
        the multiplicity parameter.
        """

        self.tb_base_message("Starting driver", 'info')

        # TODO: replace by a lock
        await Timer(100, 'ns')

        # Enable random data producer
        if self.COVERAGE_SAMPLES_OSVVM:
            self.dut.osvvm_do_stimulus <= 1
        else:
            self.dut.osvvm_do_stimulus <= 0

        self.NbEventsSent = 0

        # loop forever
        while 1:
            if not self.COVERAGE_SAMPLES_OSVVM:
                # master mode: the sw injects data into the dut
                #
                # number by events in this burst
                nb_burst_events = round(uniform(1, self.MULTIPLICITY))
                # wait a random period of time until next event
                await Timer(round(uniform(self.MIN_INTERVAL, self.MAX_INTERVAL), 0), units='ns')
                for i in range(0, nb_burst_events):
                    # send burst of nb_burst_events
                    IndexAlreadyDone = []
                    # Produce random data using random.uniform
                    channel = round(uniform(0, self.NB_CHANNELS-1))
                    data = round(uniform(0, 2**self.DATA_WIDTH-1))
                    # if this channel has already data withing this burst, move
                    # on random time to avoid fifo saturation misbehaving
                    if channel in IndexAlreadyDone:
                        await Timer(round(uniform(10, 50), 0), units='ns')
                    # inject event
                    await RisingEdge(self.dut.clk)
                    self.dut.fifo_in_data_ext <= data
                    self.dut.fifo_in_wr_ext <= 2**channel
                    await RisingEdge(self.dut.clk)
                    self.dut.fifo_in_wr_ext <= 0
                    self.dut.fifo_in_data_ext <= 0
                    # remember if this channel has already data in this burst
                    IndexAlreadyDone.append(channel)
                    # one more done
                    self.NbEventsSent += 1
            else:
                # Slave mode: wait until a new event is produced, as the vhdl is the master
                # producer. Here I only capture the data produced in there.
                await RisingEdge(self.dut.NewEvent)
                # Capture event
                channel = list(reversed(self.dut.fifo_in_wr.value)).index(1)
                data = int(self.dut.fifo_in_data.value)
                # one more done
                self.NbEventsSent += 1

            # log captured event
            self.tb_base_message("Random data produced {}, channel is {}".
                                 format(data, channel), 'info')

            # Keep track of injected values for scoring
            self.fifo_data[channel].put(data)

        self.dut.osvvm_do_stimulus <= 0
        self.tb_base_message("Stopping driver", 'info')

    async def monitor(self):
        """
        Capture output data. Check its validity.
        """

        self.tb_base_message("Starting monitor", 'info')

        self.NbEventsCaptured = 0

        # wait to start capturing data
        await RisingEdge(self.dut.wr_en)

        while 1:

            # Monitor incoming data.
            #
            # When no data arrives within a MAX_INTERVAL*1.1 time interval, test fails
            try:
                await with_timeout(FallingEdge(self.dut.fifo_out_empty), self.MAX_INTERVAL*1.1, 'ns')
            except SimTimeoutError as e:
                self.tb_base_message("No incoming data after %i ns." %
                                     (self.MAX_INTERVAL), 'warning')
                raise SimNoDataException(
                    "Missing data after {} loops ".format(self.NbEventsCaptured))

            # Capture data procedure
            await RisingEdge(self.dut.clk)
            self.dut.fifo_out_rd_en <= 1
            await RisingEdge(self.dut.clk)
            self.dut.fifo_out_rd_en <= 0
            await RisingEdge(self.dut.clk)
            data = self.dut.fifo_out_dataout.value >> 8
            channel = self.dut.fifo_out_dataout.value & 255
            self.NbEventsCaptured += 1

            # Check data validity
            data_ref = self.fifo_data[channel].get()
            if not data == data_ref:
                # raise TestFailure("{}\t error\t chanel {}\t data {}\t expected {}".
                #                   format(self.NbEventsCaptured, channel, data, data_ref))
                self.dut._log.warning("{}\t error\t chanel {}\t data {}\t expected {}".
                                      format(self.NbEventsCaptured, channel, data, data_ref))
            else:
                # all ok, log when debug
                self.dut._log.info("{}\t ok\t chanel {}\t data {}\t expected {}".
                                   format(self.NbEventsCaptured, channel, data, data_ref))

        self.tb_base_message("Stopping monitor", 'info')


# * Main test

@cocotb.test()
async def mux_fifo_test(dut):
    """
    Implements a driver / monitor test.
    Data sent by the driver is read by the monitor, checking its validity.
    """

    # create an instance of the class
    tb = mux_fifo_tb(dut)

    # log starting
    tb.tb_base_message("Start running test", 'info')

    # Wait to start testing until reset task is over
    # tb.tb_base_message(await Join(tb.reset_task), 'debug')
    tb.tb_base_message("Waiting for init reset ...", 'debug')
    await FallingEdge(dut.rst)
    tb.tb_base_message("... initial reset is over", 'debug')
    await Timer(500, units='ns')

    # start injecting data creating a thread with the driving data process
    fork(tb.driver())

    # start capturing and monitoring incoming data by threading a process
    monitor_task = fork(tb.monitor())

    # run simulation until timeout triggers
    try:
        # trigger watchdog
        await with_timeout(monitor_task, tb.TIMEOUT, 'us')
    except SimTimeoutError as e:
        # Normal test end, timeout triggers
        tb.tb_base_message("Done running test", 'info')
        tb.tb_base_message("Injected {} events, monitor {} events.".
                           format(tb.NbEventsSent, tb.NbEventsCaptured), 'info')
    except SimNoDataException as e:
        # Test failed: no incoming data
        tb.tb_base_message("{}".format(e), 'error')
        raise TestFailure

    # Display coverage reporting information
    tb.tb_base_message("Monitor at", 'info')
    dut.osvvm_do_report <= 1

    # Let some delta cycles so that the report may be issued by the simulator
    await Timer(1, units='ns')

    # print("Iscovered value is %i" % dut.iscovered.value)

    tb.tb_base_message("End running test", 'info')

    raise TestSuccess
