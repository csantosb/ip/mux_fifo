-- * Libraries

library osvvm;
-- vsg_off
package scoreboardpkg_integer is new osvvm.ScoreBoardGenericPkg

generic map (ExpectedType       => integer,
             ActualType         => integer,
             match              => std.standard."=",
             expected_to_string => to_string,
             actual_to_string   => to_string);
-- vsg_on

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.edacom_pkg.all;

library osvvm;
context osvvm.OsvvmContext;
use work.ScoreboardPkg_integer.all;

-- * Entity

entity mux_fifo_tb is
  generic (
    -- nb of parallel input streams
    g_nb_channels            : natural := 8;
    -- bus data width
    g_data_width             : natural := 16;
    -- min time int between events
    g_min_interval           : natural := 50;
    -- max time int between events
    g_max_interval           : natural := 250;
    -- seed used for random samples generator
    g_seed                   : string  := "none";
    -- Whether to produce functional coverage samples using osvvm or
    -- python uniform from random library
    g_coverage_samples_osvvm : boolean := false;
    -- Pass a debug generic to the vhdl testbench for issuing debugging
    -- reports
    g_osvvm_loglevel         : string  := "none"
    );
  port (
    -- fifo in if
    fifo_in_data_ext : in  std_logic_vector(g_data_width - 1 downto 0);
    fifo_in_wr_ext   : in  std_logic_vector(g_nb_channels - 1 downto 0);
    fifo_in_full     : out std_logic_vector(g_nb_channels - 1 downto 0);
    -- out if
    fifo_out_dataout : out std_logic_vector(g_data_width + 8 - 1 downto 0);
    fifo_out_rd_en   : in  std_logic;
    fifo_out_empty   : out std_logic
    );
end entity mux_fifo_tb;

-- * Architecture

architecture test_tb of mux_fifo_tb is

  -- ** Constants

  constant ones : std_logic_vector(g_nb_channels - 1 downto 0) := (others => '1');

  -- ** Signals

  signal newevent         : std_logic                                       := '0';
  signal clk              : std_logic                                       := '0';
  signal rst              : std_logic                                       := '0';
  signal wr_en            : std_logic                                       := '0';
  signal full             : std_logic                                       := '0';
  signal fifo_out_rd_en_r : std_logic                                       := '0';
  signal mux_fifo_dataout : std_logic_vector(g_data_width + 8 - 1 downto 0) := (others => '0');
  signal fifo_in_data     : std_logic_vector(fifo_in_data_ext'range)        := (others => '0');
  signal fifo_in_data_i   : integer                                         := 0;
  signal fifo_in_wr       : std_logic_vector(g_nb_channels - 1 downto 0)    := (others => '0');

  signal fifo_rd_if : t_fifo_rd_if_array(fifo_in_wr'range)(data(g_data_width - 1 downto 0));

  signal osvvm_do_report   : boolean := false;  -- when to do the coverage report
  signal osvvm_do_stimulus : boolean := false;  -- enable producing samples

  -- ** Alias

  -- aliases for scoreboards

  alias channel_out : std_logic_vector(7 downto 0) is fifo_out_dataout(7 downto 0);
  alias data_out    : std_logic_vector(15 downto 0) is fifo_out_dataout(23 downto 8);

  -- aliases for coverture

  alias mux_fifo_dataout_channel : std_logic_vector(7 downto 0) is mux_fifo_dataout(7 downto 0);
  alias mux_fifo_dataout_data    : std_logic_vector(g_data_width - 1 downto 0) is
    mux_fifo_dataout(mux_fifo_dataout'high downto mux_fifo_dataout'high - g_data_width + 1);

  -- ** Shared variables

  -- coverage points

  shared variable cp_dataout : covPType;
  shared variable cp_channel : covPType;

  -- scoreboards for data and channel

  shared variable sb_channel      : work.ScoreBoardPkg_integer.ScoreboardPType;
  shared variable sb_data         : work.ScoreBoardPkg_integer.ScoreboardPType;
  shared variable sb_channel_data : work.ScoreBoardPkg_integer.ScoreboardPType;

begin

  assert fifo_in_data_ext'length = g_data_width report "fifo in data has the wrong size " &
    to_string(fifo_in_data'length) &
    " instead of " &
    to_string(g_data_width) severity failure;

  assert fifo_in_wr_ext'length = fifo_in_full 'length report "signals wr_ext and full have wrong sizes" severity failure;

  fifo_in_data_i <=  to_integer(unsigned(fifo_in_data));

  -- ** Transcripts

  u_transcript : process
  begin
    -- SetTranscriptMirror;
    TranscriptOpen("./transcript.org");  -- Transcript file
    wait;
  end process u_transcript;

  -- ** Logging
  --
  -- Set log levels

  u_alert : process
  begin

    SetAlertLogName("AlertLog_Demo_Global");
    edacom_SetLogLevel(g_osvvm_loglevel);

    wait;

  end process u_alert;

  -- ** Randomized stimulus

  -- Random data to be injected to the ip

  u_stim : if g_coverage_samples_osvvm generate

    -- Produce random samples with help of osvvm

    u_osvvm_stim : process

      -- The random variables
      variable rnd_fifo_in_data : randomptype;
      variable rnd_fifo_in_wr   : randomptype;
      variable rnd_time         : randomptype;

      procedure ProduceNewEvent is
      begin
        -- enable event
        wait until rising_edge(clk);
        newevent <= '1';
        -- data
        fifo_in_data <= Rnd_fifo_in_data.Randslv(0, 2 ** g_data_width - 1, g_data_width);
        -- channel
        fifo_in_wr(Rnd_fifo_in_wr.RandInt(0, g_nb_channels - 1)) <= '1';
        -- disable event
        wait until rising_edge(clk);
        newevent                                                 <= '0';
        fifo_in_data                                             <= (others => '0');
        fifo_in_wr                                               <= (others => '0');
        -- logging
        BlankLine(1);
        Log("Random data produced by osvvm", INFO);
        Log("  Channel " & to_string(edacom_onehot_to_integer_pos(fifo_in_wr)) &
            "  Data " & to_string(fifo_in_data_i), INFO);
        if (g_osvvm_loglevel = "_info__") then
          BlankLine(1);
        end if;

      end procedure ProduceNewEvent;

    begin

      -- Initialize seeds
      Rnd_fifo_in_data.InitSeed(g_seed);
      Rnd_fifo_in_wr.InitSeed(g_seed);
      Rnd_time.InitSeed(g_seed);

      -- wait until tb enables process, and log it
      wait until osvvm_do_stimulus;
      Log("Enabled Stimulus: ", INFO);

      if (g_osvvm_loglevel = "_info__") then
        BlankLine(1);
      end if;

      -- Produce random events as long as the tb decides
      while osvvm_do_stimulus loop

        ProduceNewEvent;

        -- wait a random period of time until next event
        wait for Rnd_time.RandTime(g_min_interval * 1 ns, g_max_interval * 1 ns);

      end loop;

      Log("Disabled Stimulus: ", INFO);

      if (g_osvvm_loglevel = "_info__") then
        BlankLine(1);
      end if;

      -- stay here
      wait;

    end process u_osvvm_stim;

  else generate

    -- when not using osvvm random samples, use externally provided data

    newevent <= '1' when OneHot(fifo_in_wr_ext) else
                '0';
    fifo_in_data <= fifo_in_data_ext;
    fifo_in_wr   <= fifo_in_wr_ext;

    process (clk) is
    begin
      if clk'event and clk = '1' then
        if newevent = '1' then
          -- logging
          BlankLine(1);
          Log("Random data produced by uniform.random", INFO);
          Log("  Channel " & to_string(edacom_onehot_to_integer_pos(fifo_in_wr)) &
              "  Data " & to_string(fifo_in_data_i), INFO);
          if (g_osvvm_loglevel = "_info__") then
            BlankLine(1);
          end if;
        end if;
      end if;
    end process;

  end generate u_stim;

  -- ** Scoreboard

  -- *** Setup

  -- Configure the scoreboards

  u_sb_config : process
  begin

    sb_channel.SetName("Chanel ScoreBoard");
    sb_channel.SetAlertLogID(Name => "channel_sb");
    sb_data.SetName("Data ScoreBoard");
    sb_data.SetAlertLogID(Name    => "data_sb");
    wait;

  end process u_sb_config;

  -- *** Sample expected
  --
  -- Sample values using push method when a new event happens

  u_sb_sampling : process (clk) is
  begin

    if (clk'event and clk = '1') then
      if (newevent = '1') then
        -- sampling
        sb_channel.push(edacom_onehot_to_integer_pos(fifo_in_wr));
        sb_data.push(fifo_in_data_i);
        sb_channel_data.push("channel", edacom_onehot_to_integer_pos(fifo_in_wr));
        sb_channel_data.push("data", fifo_in_data_i);
        -- logging
        Log("ScoreBoard: sampled expected", INFO);
        Log("  Channel " & to_string(edacom_onehot_to_integer_pos(fifo_in_wr)) &
            "  Data " & to_string(fifo_in_data_i), INFO);
        if (g_osvvm_loglevel = "_info__") then
          BlankLine(1);
        end if;
      end if;
    end if;

  end process u_sb_sampling;

  -- *** Sample actual
  --
  -- capture actual values at next fifo_out_rd_en clock cycle, when the fifo outputs its contents

  u_scoreboard : process (clk) is
  begin

    if (clk'event and clk = '1') then
      fifo_out_rd_en_r <= fifo_out_rd_en;
      if (fifo_out_rd_en_r = '1') then

        if (not sb_channel_data.empty("data")) then
          sb_channel_data.check("data", to_integer(unsigned(data_out)));
        end if;
        if (not sb_channel_data.empty("channel")) then
          sb_channel_data.check("channel", to_integer(unsigned(channel_out)));
        end if;
        if (not sb_channel.empty) then
          sb_channel.check(to_integer(unsigned(channel_out)));
        end if;
        if (not sb_data.empty) then
          sb_data.check(to_integer(unsigned(data_out)));
        end if;
        -- logging
        Log("ScoreBoard: sampled actual", INFO);
        Log("  Channel " & to_string(to_integer(unsigned(channel_out))) &
            "  Data " & to_string(to_integer(unsigned(data_out))), INFO);
        if (g_osvvm_loglevel = "_info__") then
          BlankLine(1);
        end if;
      end if;
    end if;

  end process u_scoreboard;

  -- ** Coverage

  -- *** Setup
  --
  -- Create bins for histogramming cover points

  u_covbinning : process
  begin

    cp_dataout.AddBins(GenBin(0, 2 ** g_data_width - 1, g_data_width));
    cp_channel.AddBins(GenBin(0, g_nb_channels - 1));
    Log("Bins created. ", INFO);

    if (g_osvvm_loglevel = "_info__") then
      BlankLine(1);
    end if;

    wait;

  end process u_covbinning;

  -- *** Sampling
  --
  -- sample coverage data synchronously when wr_en goes high

  u_covsampling : process (clk) is
  begin

    if (clk'event and clk = '1') then
      if (wr_en = '1') then
        cp_dataout.icover(to_integer(unsigned(mux_fifo_dataout_data)));
        cp_channel.icover(to_integer(unsigned(mux_fifo_dataout_channel)));
        -- logging
        Log("Coverage: sampled data", INFO);
        Log("  Channel " & to_string(to_integer(unsigned(mux_fifo_dataout_channel))) &
            "  Data " & to_string(to_integer(unsigned(mux_fifo_dataout_data))), INFO);
        if (g_osvvm_loglevel = "_info__") then
          BlankLine(1);
        end if;
      end if;
    end if;

  end process u_covsampling;

  -- ** Reports

  -- Produce coverage and scoreboard reports

  u_reports : process
  begin

    -- wait until tb enables producing the report
    wait until osvvm_do_report;
    -- SetTranscriptMirror(false);
    BlankLine(1);

    Log("DATA Statistics:", ALWAYS);
    BlankLine(1);
    cp_dataout.writebin;

    Log("CHANNEL Statistics:", ALWAYS);
    BlankLine(1);
    cp_channel.writebin;

    BlankLine(1);

    Log("Scoreboard: channel/data errors: " & to_string(sb_channel_data.GetErrorCount) &
        "  Push counts: " & to_string(sb_channel_data.GetPushCount) &
        "  Check couts: " & to_string(sb_channel_data.GetCheckCount), ALWAYS);
    BlankLine(1);

    Log("Scoreboard: channel errors: " & to_string(sb_channel.GetErrorCount) &
        "  Push counts: " & to_string(sb_channel.GetPushCount) &
        "  Check couts: " & to_string(sb_channel.GetCheckCount), ALWAYS);
    BlankLine(1);

    Log("Scoreboard: data errors: " & to_string(sb_data.GetErrorCount) &
        "  Push counts: " & to_string(sb_data.GetPushCount) &
        "  Check couts: " & to_string(sb_data.GetCheckCount), ALWAYS);
    BlankLine(1);

    TranscriptClose;

  end process u_reports;

  -- ** Logic

  -- Logic to test

  -- *** Clock

  -- System clock

  CreateClock(clk => clk, Period => 10 ns);

  -- *** Reset

  -- System reset

  CreateReset(reset       => rst,
              resetactive => '1',
              clk         => clk,
              Period      => 1 us,
              tpd         => 0 ns);

  -- *** Set of parallel fifos

  u_fifos_in : for i in fifo_in_wr'range generate
    u_fifo_in : entity work.mux_fifo_fifo
      port map (
        wr_clk => clk,
        rst    => rst,
        din    => fifo_in_data,
        wr_en  => fifo_in_wr(i),
        full   => fifo_in_full(i),
        -- read if
        rd_clk => fifo_rd_if(i).clk,
        rd_en  => fifo_rd_if(i).rd,
        dout   => fifo_rd_if(i).data,
        empty  => fifo_rd_if(i).empty
        );

  end generate u_fifos_in;

  -- *** Ip multiplexor

  u_mux_fifo : entity work.mux_fifo
    generic map (
      g_enabled_channels => ones,
      g_extend           => 1
      )
    port map (
      rst        => rst,
      clk        => clk,
      -- fifo read
      fifo_rd_if => fifo_rd_if,
      -- fifo write
      dataout    => mux_fifo_dataout,
      wr_en      => wr_en,
      full       => full
      );

  -- *** Output fifo

  u_fifo_out : entity work.mux_fifo_fifo_out
    port map (
      wr_clk => clk,
      rst    => rst,
      din    => mux_fifo_dataout,
      wr_en  => wr_en,
      full   => full,
      --
      rd_clk => clk,
      rd_en  => fifo_out_rd_en,
      dout   => fifo_out_dataout,
      empty  => fifo_out_empty
      );

end architecture test_tb;

-- * Configuration

configuration mux_fifo_tb_test_tb_cfg of mux_fifo_tb is
  for test_tb
  end for;
end mux_fifo_tb_test_tb_cfg;
