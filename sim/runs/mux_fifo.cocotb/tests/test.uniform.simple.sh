#!/bin/sh

# restart from scratch, just in case
# make clean

# data producer is python random.uniform
OSVVM_LOGLEVEL=DEBUG LOGGING=DEBUG MIN_INTERVAL=50 MAX_INTERVAL=300 NB_CHANNELS=8 TIMEOUT=10 COVERAGE_SAMPLES_OSVVM= make -f Makefile.mux_fifo
