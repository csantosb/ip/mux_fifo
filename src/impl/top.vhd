library ieee;
use ieee.std_logic_1164.all;
use work.edacom_pkg.all;

entity top is
  generic (
    g_enabled_channels : std_logic_vector     := X"FF";
    g_extend           : natural range 0 to 1 := 1;
    --
    g_nb_channels      : natural              := 8;
    g_data_width       : natural              := 16
    );
  port (
    rst        : in    std_logic;
    clk        : in    std_logic;
    -- fifo if
    fifo_rd_if : inout t_fifo_rd_if_array(g_nb_channels - 1 downto 0)(data(g_data_width - 1 downto 0));
    -- out if
    dataout    : out   std_logic_vector(g_data_width + 8 * g_extend - 1 downto 0);
    wr_en      : out   std_logic;
    full       : in    std_logic
    );
end entity top;

architecture simple of top is

begin

  u_mux_fifo : entity work.mux_fifo
    generic map (
      g_enabled_channels => g_enabled_channels,
      g_extend           => g_extend
      )
    port map (
      rst        => rst,
      clk        => clk,
      -- fifo read
      fifo_rd_if => fifo_rd_if,
      -- fifo write
      dataout    => dataout,
      wr_en      => wr_en,
      full       => full
      );

end architecture simple;
